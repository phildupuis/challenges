# Gaming

## Back in the 90s

Bienvenue au challenge de gaming. Nostalgique des jeux que tu pouvais trouver sur Windows XP? Aujourd'hui, on déterre le classique 3D Pinball - Space Cadet, un jeu de pinball qui simule une machine à boules.

### But

Le but est simple, tu as trois balles et à l'aide des flippers tu dois les garder en jeu et faire un maximum de points en complétant les missions qui te seront assignées en jeu.

### Comment jouer

Pour y jouer, dirige toi vers l'ordinateur mis à votre disposition par le comité organisateur si la place est libre.

Le comité organisateur tiendra les comptes de qui a déjà participé pour prioriser toujours ceux qui ont moins eu la chance.

Le jeu se joue avec un contrôleur :

- les `Joysticks` vers le haut pour frapper avec un flipper;
- le bouton `A` pour actionner le lanceur;
- le `D-Pad` pour frapper sur la machine (au cas où une balle reste coincée, à ne pas abuser !).

À la fin de la partie, si les High Scores apparaissent, signalez-le à un membre du comité pour qu'il y inscrive votre nom.

Bonne chance !
