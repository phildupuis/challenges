#ifndef VEHICLEINTERFACE_H_
#define VEHICLEINTERFACE_H_

struct VehicleInterface {
    void (*startEngine)(void);
    void (*stopEngine)(void);
    void (*setSpeed)(short speedInKmh);
    void (*steer)(short angleInDegrees);
    void (*setFlashers)(_Bool status);
    unsigned int (*getAmountOfFuelInL)(void);
};

#endif

