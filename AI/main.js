const game = require("./game/game");
var elements;
var score;
var isGameOver = false;
var actionQueue = [];

startNewGame();

function movePlayer() {

}

async function startNewGame() {
    await sleep(1000);
    game.startGame();
    await sleep(500);

    while (!isGameOver) {
        await sleep(100);
        if(actionQueue.length > 0) {
            actionQueue[0]();
            actionQueue.shift();
        }
        elements = game.getElements();
        score = game.getScore();
        isGameOver = game.isGameOver();
        movePlayer();
    }
}

function goRight() {
    actionQueue.push(game.goRight);
}

function goLeft() {
    actionQueue.push(game.goLeft);
}

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}
